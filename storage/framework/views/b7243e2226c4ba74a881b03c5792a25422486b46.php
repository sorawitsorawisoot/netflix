<?php $__env->startSection('content'); ?>
    <h1>Todo</h1>
    <?php if(count($todos)>0): ?>
        <?php $__currentLoopData = $todos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $todo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div>
                <a href="<?php echo e(url('/todo/'.$todo->id)); ?>">
                <h3><?php echo e($todo->title); ?></h3>
                </a>
                <p><?php echo e($todo->content); ?></p>
                <p><?php echo e($todo->due); ?></p>
                <img src="<?php echo e(url('uploads/'.$todo->file_name)); ?>" width="129">
            </div>
            <hr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/M-THA-LAR/resources/views/index.blade.php ENDPATH**/ ?>