@extends('layouts.app')
@section('content')
    <h1>รายการโปรด</h1>
    <form method="post" action="{{ url('/todo') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>ตั้งชื่อรายการโปรดของคุณ</label>
            <input type="text" name="title" class="form-control">
        </div>
        <div class="form-group">
            <label>ชื่อเพิ่มเติม เช่น หนังที่ชื่นชอบในปีนี้</label>
            <input type="text" name="content" class="form-control">
        </div>
        <div class="form-group">
            <label>เลือกวันที่</label>
            <input type="date" name="due" class="form-control" value="{{ old('due') }}">
        </div>
        <div class="form-group">
            <label>เลือกหนังที่คุณชื่นชอบ</label>
            <input type="file" name="file" class="form-control" >
        </div>
        <button type="submit" class="btn btn-outline-success">submit</button>
    </form>
@endsection
