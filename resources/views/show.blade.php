 @extends('layouts.app')
@section('content')
    <a href="{{ url('/') }}" class="btn btn-secondary">กลับ</a>
    <h1>{{$todo->title}}</h1>
    <p>{{$todo->due}}</p>
    <hr>
    <p>{{$todo->content}}</p>
    <a href="{{ url('/todo/'.$todo->id.'/edit') }}" class="btn btn-warning">แก้ไข</a>
    <form action=" {{ url('/todo/'.$todo->id) }}" method="post" id="form-delete">
        @method('DELETE')
        @csrf
        <button class="btn btn-danger" onclick="confirm_delete()" type="button">ลบ</button>
    </form>

    <script>
        function confirm_delete() {
            var text = '{!! $todo->title !!}';
        var confirm = window.confirm('ยืนยันการลบ' + text);
        if (confirm){
            document.getElementById('form-delete').submit();
        }

        }
    </script>
@endsection
