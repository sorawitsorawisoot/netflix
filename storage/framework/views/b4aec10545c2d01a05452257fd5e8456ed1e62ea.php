 
<?php $__env->startSection('content'); ?>
    <a href="<?php echo e(url('/')); ?>" class="btn btn-secondary">กลับ</a>
    <h1><?php echo e($todo->title); ?></h1>
    <p><?php echo e($todo->due); ?></p>
    <hr>
    <p><?php echo e($todo->content); ?></p>
    <a href="<?php echo e(url('/todo/'.$todo->id.'/edit')); ?>" class="btn btn-warning">แก้ไข</a>
    <form action=" <?php echo e(url('/todo/'.$todo->id)); ?>" method="post" id="form-delete">
        <?php echo method_field('DELETE'); ?>
        <?php echo csrf_field(); ?>
        <button class="btn btn-danger" onclick="confirm_delete()" type="button">ลบ</button>
    </form>

    <script>
        function confirm_delete() {
            var text = '<?php echo $todo->title; ?>';
        var confirm = window.confirm('ยืนยันการลบ' + text);
        if (confirm){
            document.getElementById('form-delete').submit();
        }

        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sorawit208\resources\views/show.blade.php ENDPATH**/ ?>