<?php $__env->startSection('content'); ?>
    <h1>แก้ไขรายการโปรด</h1>
    <form method="post" action="<?php echo e(url('/todo/'.$todo->id)); ?>">
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <div class="form-group">
            <label>ตั้งชื่อรายการโปรดของคุณ</label>
            <input type="text" name="title" class="form-control" value="<?php echo e($todo->title); ?>">
        </div>
        <div class="form-group">
            <label>ชื่อเพิ่มเติม เช่น หนังที่ชื่นชอบในปีนี้</label>
            <input type="text" name="content" class="form-control" value="<?php echo e($todo->content); ?>">
        </div>
        <div class="form-group">
            <label>เลือกวันที่</label>
            <input type="date" name="due" class="form-control" value="<?php echo e($todo->due); ?>">
        </div>
        <button type="submit" class="btn btn-outline-success">submit</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sorawit208\resources\views/edit.blade.php ENDPATH**/ ?>