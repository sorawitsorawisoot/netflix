@extends('layouts.app')
@section('content')
    <h1>แก้ไขรายการโปรด</h1>
    <form method="post" action="{{ url('/todo/'.$todo->id) }}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>ตั้งชื่อรายการโปรดของคุณ</label>
            <input type="text" name="title" class="form-control" value="{{ $todo->title }}">
        </div>
        <div class="form-group">
            <label>ชื่อเพิ่มเติม เช่น หนังที่ชื่นชอบในปีนี้</label>
            <input type="text" name="content" class="form-control" value="{{ $todo->content }}">
        </div>
        <div class="form-group">
            <label>เลือกวันที่</label>
            <input type="date" name="due" class="form-control" value="{{ $todo->due }}">
        </div>
        <button type="submit" class="btn btn-outline-success">submit</button>
    </form>
@endsection
