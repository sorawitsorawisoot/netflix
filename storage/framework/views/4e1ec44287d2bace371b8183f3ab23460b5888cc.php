<?php $__env->startSection('content'); ?>
    <h1>Edit</h1>
    <form method="post" action="<?php echo e(url('/todo/'.$todo->id)); ?>">
        <?php echo csrf_field(); ?>
        <?php echo method_field('PUT'); ?>
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="<?php echo e($todo->title); ?>">
        </div>
        <div class="form-group">
            <label>Content</label>
            <input type="text" name="content" class="form-control" value="<?php echo e($todo->content); ?>">
        </div>
        <div class="form-group">
            <label>Due</label>
            <input type="date" name="due" class="form-control" value="<?php echo e($todo->due); ?>">
        </div>
        <button type="submit">submit</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/M-THA-LAR/resources/views/edit.blade.php ENDPATH**/ ?>