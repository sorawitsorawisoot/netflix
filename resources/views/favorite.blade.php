@extends('layouts.app')
@section('content')

    <h1>Favorite</h1>

    <nav class="navbar navbar-light">
        <h1 class="navbar-brand font-weight-bold text-danger">Netflix</h1>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>
        </form>
    </nav>

    {{--    <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>--}}

    <form method="post" action="{{ url('/todo') }}" enctype="multipart/form-data">
        @csrf

        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <img src="https://i.pinimg.com/564x/cc/ca/04/ccca04efd3d217a11084973b1cc19c12.jpg" class="card-img-top" alt="...">
                    <div class="name text-center font-weight-bold">avenger</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <img src="https://i.pinimg.com/564x/3d/d1/7a/3dd17a1dba5b7b0431a20ea5a971c982.jpg" class="card-img-top" alt="...">
                    <div class="name text-center font-weight-bold">joker</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <img src="https://i.pinimg.com/564x/70/c5/2e/70c52e479a9cf8c8b4f8e4429550ae36.jpg" class="card-img-top" alt="...">
                    <div class="name text-center font-weight-bold">star wars</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <img src="https://i.pinimg.com/564x/0e/cd/66/0ecd6689b0ec07aa15ad3c942fd8258d.jpg" class="card-img-top" alt="...">
                    <div class="name text-center font-weight-bold">jumanji</div>
                </div>
            </div>
            {{--        ----------------------------------------------}}
            <div class="col-sm-3">
                <div class="card">
                    <img src="https://i.pinimg.com/564x/c4/fc/8f/c4fc8f004b8d9d2d2a737c913719142f.jpg" class="card-img-top" alt="...">
                    <div class="name text-center font-weight-bold">dumbo</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <img src="https://i.pinimg.com/564x/10/a5/a4/10a5a43a5adcdaeebe9cdaa136ff5f39.jpg" class="card-img-top" alt="...">
                    <div class="name text-center font-weight-bold">frozen</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <img src="https://i.pinimg.com/564x/0e/a8/42/0ea84268bdf37c4112d138bf2fec40f5.jpg" class="card-img-top" alt="...">
                    <div class="name text-center font-weight-bold">x-men</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <img src="https://i.pinimg.com/564x/07/9a/c2/079ac2f459e6d03d55c64928595e13a0.jpg" class="card-img-top" alt="...">
                    <div class="name text-center font-weight-bold">guardians of the galaxy</div>
                </div>
            </div>
        </div>

        {{--        <div class="form-group">--}}
        {{--            <label>Title</label>--}}
        {{--            <input type="text" name="title" class="form-control" value="{{"thanut".rand(1,3).rand(1,3).rand(1,10).rand(1,10)}}" >--}}
        {{--        </div>--}}
        {{--        <div class="form-group">--}}
        {{--            <label>Content</label>--}}
        {{--            <input type="text" name="content" class="form-control" value="{{ "thanut".rand(1,3).rand(1,3).rand(1,10).rand(1,10)}}">--}}
        {{--        </div>--}}
        {{--        <div class="form-group">--}}
        {{--            <label>Due</label>--}}
        {{--            <input type="date" name="due" class="form-control" value="{{ old('due') }}">--}}
        {{--        </div>--}}
        {{--        <div class="form-group">--}}
        {{--            <label>Due</label>--}}
        {{--            <input type="file" name="file" class="form-control" >--}}
        {{--        </div>--}}
        {{--        <button type="submit">submit</button>--}}
    </form>
@endsection
