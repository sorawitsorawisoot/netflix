<?php $__env->startSection('content'); ?>
    <a href="<?php echo e(url('/')); ?>" class="btn btn-secondary">Go Back</a>
    <h1><?php echo e($todo->title); ?></h1>
    <p><?php echo e($todo->due); ?></p>
    <hr>
    <p><?php echo e($todo->content); ?></p>
    <form action=" <?php echo e(url('/todo/'.$todo->id)); ?>" method="post" id="form-delete">
        <?php echo method_field('DELETE'); ?>
        <?php echo csrf_field(); ?>
        <button class="btn btn-danger" onclick="confirm_delete()" type="button">Delete</button>
    </form>
    <a href="<?php echo e(url('/todo/'.$todo->id.'/edit')); ?>" class="btn btn-primary">Edit</a>
    <script>
        function confirm_delete() {
            var text = '<?php echo $todo->title; ?>';
        var confirm = window.confirm('นั่งยันการลบ' + text);
        if (confirm){
            document.getElementById('form-delete').submit();
        }

        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/M-THA-LAR/resources/views/show.blade.php ENDPATH**/ ?>