<?php $__env->startSection('content'); ?>

    <nav class="navbar navbar-light">
        <h1 class="navbar-brand font-weight-bold text-danger">Netflix</h1>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>
        </form>
    </nav>

    <a class="btn btn-outline-success" href="/todo/create" role="button">+ เพิ่มรายการโปรดของคุณ</a>

    <div class="row">
        <div class="col-sm-3">
            <div class="card">
                <img src="https://i.pinimg.com/564x/cc/ca/04/ccca04efd3d217a11084973b1cc19c12.jpg" class="card-img-top" alt="...">
                <div class="name text-center font-weight-bold">avenger</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <img src="https://i.pinimg.com/564x/3d/d1/7a/3dd17a1dba5b7b0431a20ea5a971c982.jpg" class="card-img-top" alt="...">
                <div class="name text-center font-weight-bold">joker</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <img src="https://i.pinimg.com/564x/70/c5/2e/70c52e479a9cf8c8b4f8e4429550ae36.jpg" class="card-img-top" alt="...">
                <div class="name text-center font-weight-bold">star wars</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <img src="https://i.pinimg.com/564x/0e/cd/66/0ecd6689b0ec07aa15ad3c942fd8258d.jpg" class="card-img-top" alt="...">
                <div class="name text-center font-weight-bold">jumanji</div>
            </div>
        </div>
        
        <div class="col-sm-3">
            <div class="card">
                <img src="https://i.pinimg.com/564x/c4/fc/8f/c4fc8f004b8d9d2d2a737c913719142f.jpg" class="card-img-top" alt="...">
                <div class="name text-center font-weight-bold">dumbo</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <img src="https://i.pinimg.com/564x/10/a5/a4/10a5a43a5adcdaeebe9cdaa136ff5f39.jpg" class="card-img-top" alt="...">
                <div class="name text-center font-weight-bold">frozen</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <img src="https://i.pinimg.com/564x/0e/a8/42/0ea84268bdf37c4112d138bf2fec40f5.jpg" class="card-img-top" alt="...">
                <div class="name text-center font-weight-bold">x-men</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <img src="https://i.pinimg.com/564x/07/9a/c2/079ac2f459e6d03d55c64928595e13a0.jpg" class="card-img-top" alt="...">
                <div class="name text-center font-weight-bold">guardians of the galaxy</div>
            </div>
        </div>
    </div>

    <h2>รายการโปรด</h2>
    <?php if(count($todos)>0): ?>
        <?php $__currentLoopData = $todos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $todo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div>
                <a href="<?php echo e(url('/todo/'.$todo->id)); ?>">
                    <h3><?php echo e($todo->title); ?></h3>
                </a>
                <p><?php echo e($todo->content); ?></p>
                <p><?php echo e($todo->due); ?></p>
                <img src="<?php echo e(url('uploads/'.$todo->file_name)); ?>" width="129">
            </div>
            <hr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sorawit208\resources\views/index.blade.php ENDPATH**/ ?>